# duckdb-estudos


Criei esse repositório para concentrar os scripts e exemplos que estou estudando e para facilitar a vida de outros que estejam na mesma jornada. :)

# Site Oficial
https://duckdb.org/

## Documentação 
https://duckdb.org/docs/

## Referência para estudo
https://duckdb.org/docs/api/python/overview

## Datasets utilizados
https://www.kaggle.com/datasets/jasmeet0516/bird-flu-dataset-avian-influenza
https://www.kaggle.com/datasets/zynicide/nfl-football-player-stats